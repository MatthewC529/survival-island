﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Island.Object
{
    public abstract class BasicObject : MonoBehaviour
    {

        /*
         * Abstract Base Class for Any Object We Use... Woo Polymorphism.
         * More Detailed Documentation to Come.
         * 
         * Authors: Matt C.
         * Last Edit: Matt C. -- 3/7/14 - 01:14 -- Initial Creation and Work
         * 
         */ 

        private int defaultindex = 0;

        private string name;
        private GameObject entity;
        private Dictionary<String, Animation> Animations = new Dictionary<string, Animation>();

        private BasicInventory inventory = null;
        public static List<BasicObject> masterList = new List<BasicObject>();

        public BasicObject(string name, string prefabName, Vector3 position = new Vector3(), Quaternion rot = new Quaternion())
        {
            this.name = name;
            if (prefabName == null) {
                DestroyImmediate(this);
                throw new Exception("FAILURE: GameObject of name, " + name + " will not be created! No Prefab Attached!");
            }

            entity = (GameObject)Instantiate(Resources.Load(prefabName));
            entity.transform.position = position;
            entity.transform.rotation = rot;

        }

        public virtual void AssignInventory(BasicInventory inventory)
        {
            Debug.Log("[Failed Attempt] -- This Object Does Not Override the AssignInventory Function in BasicObject! By Default it Can NOT Take an Inventory!");
        }

        public void AddAnimation(Animation anim, String name = "")
        {
            if (name.Length < 1) { name = "Default_" + defaultindex; defaultindex++; }
            Animations.Add(name, anim);
        }

        public void RemoveAnimation(String name = "")
        {
            try {
                Animations.Remove(name);
            } catch (Exception e) {
                Debug.Log("EXCEPTION THROWN WHEN REMOVING ANIMATION OF NAME, " + name + "!");
                Debug.LogException(e);
            }
        }

        public GameObject Entity
        {
            get { return Entity; }
        }

        public BasicInventory Inventory
        {
            get { return inventory; }
            set { AssignInventory(value); }
        }

        public String Name
        {
            get
            {
                if (this.GetType() == typeof(Player)) {
                    return name.Length > 0 ? String.Format("Player {0}", name) : "Player";
                } else {
                    return name;
                }
            }

            set
            {
                if (this.GetType() != typeof(Player))
                    name = value;
                else
                    name = String.Format("Player - {0}", value);
            }
        }

    }
}