﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Island.Object
{
    interface Holdable
    {

        Boolean IsHeld();
        String GetName();
        void SetName();
        void pickUp();
        void drop();
        void use();

    }
}
