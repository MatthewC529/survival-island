﻿using UnityEngine;
using System;
using System.Collections;

namespace Island.Utils
{
    public class MouseButton
    {
        public const int LeftMouseButton = 0;
        public const int RightMouseButton = 1;
        public const int MiddleMouseButton = 2;
    }
}