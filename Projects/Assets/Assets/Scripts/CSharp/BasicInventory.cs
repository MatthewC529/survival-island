﻿using UnityEngine;
using System;
using Island.Object;
using System.Collections.Generic;

namespace Island
{
    [Serializable]
    public class BasicInventory : MonoBehaviour
    {

        /*****************************************************************
         * NOTES:
         * 
         * This is an extremely simplistic inventory system. If you look at the code carefully,
         * you'll notice some drawbacks. Firstly, there is no limit on how many items you can carry.
         * I know that we will have to introduce a limit somehow, but since there is no words yet on
         * what that limit should be, I haven't implement it.
         * 
         * The second is that the RemoveItem() method uses .NET's builtin remove mthod, which
         * will remove the first occurence of the item. So, if we have multiple items of the same type,
         * it is possible that it might pull the wrong item simply because of its index. I'm considering
         * to change it to a key-value pair, but I'm waiting for a discussion.
         * 
         * Authors: Mario P., Matt C.
         * Last Edit: Matt C. -- 3/7/14 - 02:11 -- Documentation and Optimization
         * 
         *****************************************************************/

        //Global Flag to Turn Off Inventory. Why? 'Cause.
        public Boolean enabled = true;
        public int InventoryCap = -1;                       //Inventory Capacity, Right now -1 == No Cap
        private BasicObject Holder;

        // Adjustable Generic List to Hold Inventory
        private List<BasicObject> playerInventory = new List<BasicObject>();

		public BasicInventory(BasicObject holder)
        {
            this.Holder = holder;
        }

        /// <summary>
        /// Adds Given GameObject to Item
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(BasicObject item)
        {
            if (!enabled) return;

            if (playerInventory.Count <= InventoryCap || InventoryCap == -1)
                playerInventory.Add(item);
        }

        /// <summary>
        /// Remove Item from the Entity's Inventory
        /// </summary>
        /// <param name="item"></param>
        public void RemoveItem(BasicObject item)
        {
            playerInventory.Remove(item);
        }


        private void OnGUI()
        {
            string inventoryGUI = "";

            if (playerInventory.Count <= 0 || !enabled) {                                   //If Inventory Size <= 0 or Inventory is Disabled
                inventoryGUI = "No Inventory";                                              //Inform Player There is No Inventory (Temp until we get a GUI)
            } else {
                int count = playerInventory.Count;                                          //Just story the Inv. Size in a Count Variable
                inventoryGUI = "You have ";                                                 //Start Inventory Prompt
                StringifyInventory(ref inventoryGUI);                                       //Add Inventory to inventoryGUI String
                inventoryGUI += "in your inventory!";                                       //Finish Prompt

            }

            GUI.Label(new Rect(100, 10, 100, 20), inventoryGUI); // Display inventory message onscreen

        }

        private void StringifyInventory(ref String attache)
        {
            int count = playerInventory.Count;
            for (int i = 0; i < count; i++) {                                           //Start Iteration through Inventory
                if (i != count - 1) attache += playerInventory[i].name + ", ";     //If i < Size - 1 (Lists, like Arrays start at 0 but .Count doesnt take that into account) add Item name to prompt
                else attache += playerInventory[i].name + " ";                     //else (basically if i == size - 1) then add the item name but do not add the ', '
            }
        }

        public List<BasicObject> Inventory
        {
            get { return playerInventory; }
            set { playerInventory = value; }
        }

        public Boolean IsEnabled()
        {
            return enabled;
        }

        public void SetEnabled(Boolean enabled)
        {
            this.enabled = enabled;
        }

        public override string ToString()
        {
            String str = "Inventory Containing ";
            StringifyInventory(ref str);
            return str;
        }
    }
}
