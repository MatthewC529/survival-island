﻿/// <summary>
/// Interfaces namespace should contain any Interface used in Survivor Island.
/// </summary>
namespace Interfaces {
	/// <summary>
	/// IEventCallable is a generic interface used in GENERIC classes which desire to be called on event queues...
	/// </summary>
	public interface IEventCallable {
		/// <summary>
		/// Called every 6 seconds.
		/// </summary>
		/// <param name="CalleeData">A reference to the callee data.</param>
		/// <returns>A reference to the modified callee data after the heartbeat has occured.</returns>
		object OnHeartbeat(object CalleeData);
	}
}