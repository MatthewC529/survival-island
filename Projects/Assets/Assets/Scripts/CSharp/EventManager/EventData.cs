﻿using System;

/// <summary>
/// Events namespace contains code pertaining to the event manager.
/// </summary>
namespace Events{
	public partial class EventManager{
		/// <summary>
		/// EventData is the internal currency of the event manager.
		/// </summary>
		public struct EventData {
			/// <summary>
			/// The caller of the event, should be "this" when creating a new event.
			/// </summary>
			public object Caller;
			/// <summary>
			/// The callee of the event, should be the target on which the event will occur.
			/// </summary>
			public Interfaces.IEventCallable Callee;
			/// <summary>
			/// Generic data to be handed to the callee on the call.
			/// </summary>
			public object CalleeData;
			/// <summary>
			/// The call type, such as a heartbeat call, or other...
			/// </summary>
			public Enumerations.EventType CallType;
			/// <summary>
			/// The time which the call should commence, this is relative to UtcNow (better for network, if we latter choose to incorporate such).
			/// </summary>
			public System.DateTime CallTime;
			/// <summary>
			/// A log message to store to a log when the event is fired, this should identify the call precisly. Even using a numeric hash is better than nothing here!
			/// </summary>
			public string CallLogMessage;
		}
	}
}