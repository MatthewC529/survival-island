﻿namespace Events{
	/// <summary>
	/// The Event Manager Class handles the in game Events, see the exposed Methods for more information.
	/// </summary>
	public partial class EventManager{
		private class EventQueue
		{
			private class EventQueueData
			{
				public EventData Data;
				public EventQueueData Next;
				public EventQueueData Previous;
				public EventQueueData(EventData Data, EventQueueData Previous, EventQueueData Next)
				{ this.Data = Data; this.Next = Next; this.Previous = Previous; }
			}
			private EventQueueData EventQueueListHead;
			/// <summary>
			/// EventQueue Constructor, Clears the EventQueue.
			/// </summary>
			public EventQueue()
			{
				this.Clear();
			}
			/// <summary>
			/// Clears the EventQueue, Destroying any Events stored in it.
			/// </summary>
			public void Clear()
			{
				this.EventQueueListHead = null;
			}
			/// <summary>
			/// Inserts an Event into the linear CallTime Position, ensuring that events are called in correct order.
			/// </summary>
			/// <param name="Data">The event to be inserted into the Queue.</param>
			public void Insert(EventData Data)
			{
				if (this.EventQueueListHead == null)
				{
					this.EventQueueListHead = new EventQueueData(Data, null, null);
				}
				else
				{
					EventQueueData Current = EventQueueListHead;
					while (Current.Next != null)
					{
						if (Data.CallTime.Ticks < Current.Data.CallTime.Ticks) break;
						Current = Current.Next;
					}
					EventQueueData New = new EventQueueData(Data, Current.Previous, Current);
					EventQueueData Previous = Current.Previous;
					if (Previous != null)
						Previous.Next = New;
					Current.Previous = New;
				}
			}
			/// <summary>
			/// Consumes an event from the event queue and returns the event data.
			/// </summary>
			/// <returns>The EventData.</returns>
			public EventData Consume()
			{
				EventData Result = this.EventQueueListHead.Data;
				this.EventQueueListHead = this.EventQueueListHead.Next;
				this.EventQueueListHead.Previous = null;
				return Result;
			}
			/// <summary>
			/// Allows a peek at the next Event without consuming it.
			/// </summary>
			public EventData Peek()
			{
				return this.EventQueueListHead.Data;
			}
		}
		private static EventQueue Queue;
		/// <summary>
		/// Creates a new event on the event queue.
		/// </summary>
		/// <param name="Data">The event to create.</param>
		public static void NewEvent(EventData Data)
		{
			EventManager.Queue.Insert(Data);
		}
		/// <summary>
		/// Consumes N=20 events, executing them in order, or breaking and returning if no events are ready to executed yet.
		/// </summary>
		public static void ConsumeAndExecute()
		{
			byte cnt = 19;
			while (cnt-- > 0)
			{
				if (EventManager.Queue.Peek().CallTime.Ticks > System.DateTime.UtcNow.Ticks) break;
				EventData Event=EventManager.Queue.Consume();
				switch (Event.CallType)
				{
				case Enumerations.EventType.HeartBeat:
					Event.CalleeData=Event.Callee.OnHeartbeat(Event.CalleeData);
					Event.CallTime=System.DateTime.UtcNow+System.TimeSpan.FromSeconds(6);
					EventManager.Queue.Insert(Event);
					break;
				}
			}
		}
	}
}