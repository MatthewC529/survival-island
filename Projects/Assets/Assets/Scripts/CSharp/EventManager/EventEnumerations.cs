﻿/// <summary>
/// Enumeration namespace contains the Enumerations used in Survivor Island.
/// </summary>
namespace Enumerations{
	/// <summary>
	/// The EventTypes that can occur through out the game of Survivor Island.
	/// </summary>
	public enum EventType{
		/// <summary>
		/// The HeartBeat Event Type.*** NOTE - There should only be ONE HB per Object! ***
		/// </summary>
		/// <remarks>Don't ABUSE the On HB Scripts, they will be the most intensive part of Survivor Island, and excessive calls will produce too much overhead...</remarks>
		HeartBeat=0
	}
}