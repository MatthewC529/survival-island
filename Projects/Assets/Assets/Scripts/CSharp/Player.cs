﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Island;
using Island.Object;

namespace Island.Object
{
    /*TODO
     * 
     * Authors: Matt C.
     */

    class Player : BasicObject
    {

        public Player(String name, String prefabName, Vector3 position, Quaternion rot = new Quaternion()) : base(name, prefabName, position, rot)
        {
            base.Inventory = new BasicInventory(this);
        }

        public override void AssignInventory(BasicInventory inventory)
        {
            if (base.Inventory.IsEnabled()) {
                base.Inventory = inventory;
            }else{
                inventory.SetEnabled(false);
                base.Inventory = inventory;
            }
        }
    }
}
