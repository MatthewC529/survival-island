﻿using UnityEngine;
using UnityEngine.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using Island.Utils;
using Island.Object;

public class Targeting : MonoBehaviour {

    public float MaxTargetDistance = 5f;            //Basically, How Far The Character Can Reach
    public int TargetLayer = Physics.AllLayers;     //Unity has Layers, This will detect ALL Objects, This can be modified to a specific layer ,say for entities.
    public BasicObject Object;                      //The Object the Targeting System is tied to, Typically this is the player.

	private string debugMessage = "No object detected"; // Debug message so it's still visible in a build run

    [NonSerialized]
	private RaycastHit target;                      //Data from a Raycast done by Physics.raycast

	// Use this for initialization
	void Start () {}

	public void PickUp (BasicObject item)
	{
		Object.Inventory.AddItem(item);
	}

	void OnGUI () {
		GUI.Label (new Rect (50, 20, 200, 20), debugMessage);

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(MouseButton.RightMouseButton)) {
			// DO a raycast and see if it hits
			if (Physics.Raycast(transform.position, transform.forward, out target, MaxTargetDistance, TargetLayer)) {
				debugMessage = "An object is detected:" + target.transform.tag; // If it does, say what it hits
			} else {
				debugMessage = "No object detected";
			}
		}
	}
}
